#include QMK_KEYBOARD_H

#ifdef PROTOCOL_LUFA
  #include "lufa.h"
  #include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif

extern uint8_t is_master;

enum {
  SPC_TO_ENTER = 0,
  SLASH_COMMENT,
};

qk_tap_dance_action_t tap_dance_actions[] = {
  [SPC_TO_ENTER] = ACTION_TAP_DANCE_DOUBLE(KC_SPC, KC_ENT),
  [SLASH_COMMENT] = ACTION_TAP_DANCE_DOUBLE(KC_SLSH, LCTL(KC_SLSH))
};

enum layer_number {
  _QWERTY = 0,
  _LOWER,
  _RAISE,
  _ADJUST,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

/* QWERTY
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * | ESC  |   1  |   2  |   3  |   4  |   5  |                    |   6  |   7  |   8  |   9  |   0  |  -   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * | Tab  |   Q  |   W  |   E  |   R  |   T  |                    |   Y  |   U  |   I  |   O  |   P  |  =   |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |LShift|   A  |   S  |   D  |   F  |   G  |-------.    ,-------|   H  |   J  |   K  |   L  |   ;  |  '   |
 * |------+------+------+------+------+------|   [   |    |    ]  |------+------+------+------+------+------|
 * |LCTRL |   Z  |   X  |   C  |   V  |   B  |-------|    |-------|   N  |   M  |   ,  |   .  |   /  |RShift|
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LGUI |LOWER | LAlt | /Space  /       \Space \ |BackSP|RAISE| Enter|
 *                   `-----------------------------'         '------''-------------------'
 */

 [_QWERTY] = LAYOUT( \
  KC_GESC,   KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                      KC_6,    KC_7,    KC_8,    KC_9,    KC_0,     KC_MINS, \
  KC_TAB,   KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,      KC_EQL, \
  KC_LSFT,  KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                      KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,  KC_QUOT, \
  KC_LCTL,  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_LBRC,  KC_RBRC,   KC_N,    KC_M,    KC_COMM, KC_DOT,  TD(SLASH_COMMENT),  KC_RSFT, \
                       KC_LALT, MO(_LOWER), KC_LGUI, KC_SPC,   TD(SPC_TO_ENTER), KC_BSPC, MO(_RAISE), KC_ENT \
),
/* LOWER
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |   `  |      |      |      |      |      |                    |      |      |      |      |      |PrtScr|
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |  <<  | Stop | Play |  >>  | Vol- | Vol+ |                    |      |  LK  |  M^  |  RK  |  CK  |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |   |  |   !  |   @  |   #  |   $  |   %  |-------.    ,-------|   ^  |  M<  |  MD  |  M>  |   )  |   -  |
 * |------+------+------+------+------+------|   {   |    |   }   |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |      |      |      |      |      |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LGUI |LOWER | LAlt | /Space  /       \Space \ |  Del | RAISE | Enter |
 *                   `-----------------------------'         '------''----------------------'
 */
[_LOWER] = LAYOUT( \
   KC_GRV, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, TO(_ADJUST),              XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PSCR, \
  KC_MPRV, KC_MSTP, KC_MPLY, KC_MNXT, KC_VOLD, KC_VOLU,                  XXXXXXX, KC_BTN1, KC_MS_U, KC_BTN2, KC_BTN3, XXXXXXX, \
  KC_PIPE, KC_EXLM,   KC_AT, KC_HASH,  KC_DLR, KC_PERC,                  KC_CIRC, KC_MS_L, KC_MS_D, KC_MS_R, KC_RPRN, KC_TILD, \
  _______, _______, _______,_______, _______, _______, KC_LCBR, KC_RCBR, _______, _______, _______, _______, _______, _______, \
                             _______, TG(_LOWER), _______, _______, _______, KC_DEL, TO(_RAISE), _______\
),
/* RAISE
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |   `  |      |      |  F1  |  F2  |  F3  |                    |   6  |   7  |   8  |   9  |   0  |PrnScr|
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |  F4  |  F5  |  F6  |                    | PgUp | Home |  Up  | End  |  (   |   )  |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |  F7  |  F8  |  F9  |-------.    ,-------|PgDown| Left | Down | Right|  {   |   }  |
 * |------+------+------+------+------+------|   (   |    |   )   |------+------+------+------+------+------|
 * |      |      |      | F10  | F11  | F12  |-------|    |-------|   +  |   -  |   =  |   \  |  [   |   ]  |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LGUI |LOWER | LAlt | /Space  /       \Space \ |  Del |RAISE | Enter|
 *                   `----------------------------'          '------''--------------------'
 */

[_RAISE] = LAYOUT( \
   KC_GRV, XXXXXXX, XXXXXXX,  KC_F1,  KC_F2,  KC_F3,                       KC_6,    KC_7,    KC_8,    KC_9,    KC_0, LSFT(LCTL(KC_PSCR)), \
  _______, XXXXXXX, XXXXXXX,  KC_F4,  KC_F5,  KC_F6,                    KC_PGUP, KC_HOME,   KC_UP,  KC_END, KC_LPRN, KC_RPRN, \
  _______, XXXXXXX, XXXXXXX,  KC_F7,  KC_F8,  KC_F9,                    KC_PGDN, KC_LEFT, KC_DOWN, KC_RGHT, KC_LCBR, KC_RCBR, \
  _______, XXXXXXX, XXXXXXX, KC_F10, KC_F11, KC_F12,  KC_LPRN, KC_RPRN, KC_PLUS, KC_MINS,  KC_EQL, KC_BSLS, KC_LBRC, KC_RBRC, \
                                 _______, TO(_LOWER), _______, _______, _______,  KC_DEL, TG(_RAISE),  _______ \
),
/* ADJUST
 * ,-----------------------------------------.                    ,-----------------------------------------.
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |                    |      |      |      |      |      |      |
 * |------+------+------+------+------+------|                    |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------.    ,-------|      |      |RGB ON| HUE+ | SAT+ | VAL+ |
 * |------+------+------+------+------+------|       |    |       |------+------+------+------+------+------|
 * |      |      |      |      |      |      |-------|    |-------|      |      | MODE | HUE- | SAT- | VAL- |
 * `-----------------------------------------/       /     \      \-----------------------------------------'
 *                   | LAlt | LGUI |LOWER | /Space  /       \Enter \  |RAISE |BackSP| RGUI |
 *                   |      |      |      |/       /         \      \ |      |      |      |
 *                   `----------------------------'           '------''--------------------'
 */
  [_ADJUST] = LAYOUT( \
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,\
                             _______, TG(_ADJUST), _______, _______, _______,  _______, _______, _______ \
  )
};

// Setting ADJUST layer RGB back to default
void update_tri_layer_RGB(uint8_t layer1, uint8_t layer2, uint8_t layer3) {
  if (IS_LAYER_ON(layer1) && IS_LAYER_ON(layer2)) {
    layer_on(layer3);
  } else {
    layer_off(layer3);
  }
}

//SSD1306 OLED update loop, make sure to enable OLED_DRIVER_ENABLE=yes in rules.mk
#ifdef OLED_DRIVER_ENABLE

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_keyboard_master())
    { return OLED_ROTATION_180; }  // flips the display 180 degrees if offhand
  return OLED_ROTATION_270;
}

// When you add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
void set_keylog(uint16_t keycode, keyrecord_t *record);
const char *read_keylog(void);
const char *read_keylogs(void);

//const char *read_mode_icon(bool swap);
// const char *read_host_led_state(void);
//void set_timelog(void);
//const char *read_timelog(void);
char wpm[7];

void oled_task_user(void) {
  if (is_keyboard_master()) {
    // If you want to change the display of OLED, you need to change here
    oled_write_ln(read_layer_state(), false);
    oled_write_ln(" wpm", false);
    sprintf(wpm, " %03d \n", get_current_wpm());
    oled_write(wpm, false);

    oled_write_ln(read_keylog(), false);
  
    // oled_write_ln(read_timelog(), false);
   
    // oled_write_ln(read_mode_icon(keymap_config.swap_lalt_lgui), false);
    oled_write_ln(read_keylogs(), false);
    
    //oled_write_ln(read_host_led_state(), false);
    
  } else {
    // oled_write_ln(read_layer_state(), false);
    // oled_write_ln(read_mode_icon(keymap_config.swap_lalt_lgui), false);
    // oled_write_ln(read_keylogs(), false);
    // oled_write_ln(read_host_led_state(), false);
    // oled_write_ln(read_timelog(), false);
    oled_write(read_logo(), false);
  }
}
#endif // OLED_DRIVER_ENABLE

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
#ifdef OLED_DRIVER_ENABLE
    set_keylog(keycode, record);
    //set_timelog();
#endif
    
  }
  return true;
}