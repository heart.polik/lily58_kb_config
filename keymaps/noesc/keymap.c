#include QMK_KEYBOARD_H

#ifdef PROTOCOL_LUFA
  #include "lufa.h"
  #include "split_util.h"
#endif
#ifdef SSD1306OLED
  #include "ssd1306.h"
#endif

extern uint8_t is_master;

enum {
  SPC_TO_ENTER = 0,
  SLASH_COMMENT,
};

qk_tap_dance_action_t tap_dance_actions[] = {
  [SPC_TO_ENTER] = ACTION_TAP_DANCE_DOUBLE(KC_SPC, KC_ENT),
  [SLASH_COMMENT] = ACTION_TAP_DANCE_DOUBLE(KC_SLSH, LCTL(KC_SLSH))
};

enum layer_number {
  _QWERTY = 0,
  _LOWER,
  _RAISE,
  _ADJUST,
};

// Light LEDs 6 to 9 and 12 to 15 red when caps lock is active. Hard to ignore!
const rgblight_segment_t PROGMEM rgb_qwerty[] = RGBLIGHT_LAYER_SEGMENTS(
    // {5, 1, HSV_YELLOW},       // Light 4 LEDs, starting with LED 6
    // {6, 1, HSV_BLUE},   
    {17, 1, HSV_GREEN},
    {47, 1, HSV_GREEN},
    {18, 1, HSV_RED},
    {26, 1, HSV_MAGENTA},
    // {27, 1, HSV_GOLD}
    {28, 1, HSV_BLUE}
         // Light 4 LEDs, starting with LED 12
);
// Light LEDs 9 & 10 in cyan when keyboard layer 1 is active
const rgblight_segment_t PROGMEM rgb_lower[] = RGBLIGHT_LAYER_SEGMENTS(
    {0, 1, HSV_YELLOW},
    {5, 1, HSV_AZURE},
    {12, 6, HSV_GOLD},
    {34, 1, HSV_RED},
    {41, 6, HSV_GOLD}
);
// Light LEDs 11 & 12 in purple when keyboard layer 2 is active
const rgblight_segment_t PROGMEM rgb_upper[] = RGBLIGHT_LAYER_SEGMENTS(
    {5, 0, HSV_AZURE},
    {0, 5, HSV_GREEN},
    {25, 1, HSV_GREEN},
    {29, 5, HSV_GREEN},
    {54, 1, HSV_GREEN},  //f1-f12
    {34, 1, HSV_RED},
    {38, 1, HSV_RED},
    {42, 3, HSV_RED}, //arrows
    {37, 1, HSV_BLUE},
    {39, 1, HSV_BLUE}, // home-end
    {40, 2, HSV_MAGENTA}, // pgup-down 
    {35, 2, HSV_WHITE},
    {45, 4, HSV_WHITE},
    {49, 4, HSV_YELLOW}

);
const rgblight_segment_t PROGMEM rgb_adjust[] = RGBLIGHT_LAYER_SEGMENTS(
    {5, 1, HSV_WHITE},
    {1, 3, HSV_RED},
    {4, 1, HSV_MAGENTA},
    {8, 1, HSV_GREEN},
    {9, 1, HSV_WHITE},
    {27, 1, HSV_BLUE}
);

// Now define the array of layers. Later layers take precedence
const rgblight_segment_t* const PROGMEM my_rgb_layers[] = RGBLIGHT_LAYERS_LIST(
    rgb_qwerty,
    rgb_lower,    // Overrides caps lock layer
    rgb_upper,
    rgb_adjust     // Overrides other layers
);

void keyboard_post_init_user(void) {
    // Enable the LED layers
    rgblight_layers = my_rgb_layers;
}

layer_state_t layer_state_set_user(layer_state_t state) {
    // Both layers will light up if both kb layers are active
    rgblight_set_layer_state(0, layer_state_cmp(state, 0));
    rgblight_set_layer_state(1, layer_state_cmp(state, 1));
    rgblight_set_layer_state(2, layer_state_cmp(state, 2));
    rgblight_set_layer_state(3, layer_state_cmp(state, 3));
    return state;
}

// bool led_update_user(led_t led_state) {
//     rgblight_set_layer_state(0, led_state.caps_lock);
//     return true;
// }

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

[_QWERTY] = LAYOUT( \
  KC_GESC,  KC_1,   KC_2,    KC_3,    KC_4,    KC_5,                      KC_6,    KC_7,    KC_8,    KC_9,    KC_0,     KC_MINS, \
  KC_TAB,   KC_Q,   KC_W,    KC_E,    KC_R,    KC_T,                      KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,      KC_EQL, \
  KC_LSFT,  KC_A,   KC_S,    KC_D,    KC_F,    KC_G,                      KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN,  KC_QUOT, \
  KC_LCTL,  KC_Z,   KC_X,    KC_C,    KC_V,    KC_B, KC_LBRC,  KC_RBRC,   KC_N,    KC_M,    KC_COMM, KC_DOT,  TD(SLASH_COMMENT),  KC_RSFT, \
                       KC_LALT, MO(_LOWER), KC_LGUI, KC_SPC,   TD(SPC_TO_ENTER), KC_BSPC, MO(_RAISE), KC_ENT \
),
[_LOWER] = LAYOUT( \
   KC_GRV, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, TO(_ADJUST),              XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, KC_PSCR, \
  KC_MPRV, KC_MSTP, KC_MPLY, KC_MNXT, KC_VOLD, KC_VOLU,                  XXXXXXX, KC_BTN1, KC_MS_U, KC_BTN2, KC_BTN3, XXXXXXX, \
  KC_PIPE, KC_EXLM,   KC_AT, KC_HASH,  KC_DLR, KC_PERC,                  KC_CIRC, KC_MS_L, KC_MS_D, KC_MS_R, KC_RPRN, KC_TILD, \
  _______, _______, _______,_______, _______, _______, KC_LCBR, KC_RCBR, _______, _______, _______, _______, _______, _______, \
                             _______, TG(_LOWER), _______, _______, _______, KC_DEL, TO(_RAISE), _______\
),
[_RAISE] = LAYOUT( \
   KC_GRV,   KC_F1,   KC_F2,    KC_F3,    KC_F4,    KC_F5,                      KC_F6,   KC_F7,   KC_F8,    KC_F9,  KC_F10, LSFT(LCTL(KC_PSCR)), \
  _______, XXXXXXX, XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,                    KC_PGUP, KC_HOME,   KC_UP,  KC_END, KC_LPRN, KC_RPRN, \
  _______, XXXXXXX, XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,                    KC_PGDN, KC_LEFT, KC_DOWN, KC_RGHT, KC_LCBR, KC_RCBR, \
  _______, XXXXXXX, XXXXXXX,  XXXXXXX,  XXXXXXX,  XXXXXXX,  KC_LPRN, KC_RPRN, KC_PLUS, KC_MINS,  KC_EQL, KC_BSLS, KC_LBRC, KC_RBRC, \
                               _______, TO(_LOWER), _______, KC_F11,  KC_F12,  KC_DEL, TG(_RAISE),  _______ \
),
[_ADJUST] = LAYOUT( \
  RGB_TOG, RGB_MOD, RGB_HUI, RGB_SAI, RGB_VAI, RGB_SPI,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
  XXXXXXX, RGB_M_T, RGB_HUD, RGB_SAD, RGB_VAD, RGB_SPD,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,                   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, \
  XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,\
                             _______, TG(_ADJUST), _______, _______, _______,  _______, _______, _______ \
  )
};

// Setting ADJUST layer RGB back to default
void update_tri_layer_RGB(uint8_t layer1, uint8_t layer2, uint8_t layer3) {
  if (IS_LAYER_ON(layer1) && IS_LAYER_ON(layer2)) {
    layer_on(layer3);
  } else {
    layer_off(layer3);
  }
}

//SSD1306 OLED update loop, make sure to enable OLED_DRIVER_ENABLE=yes in rules.mk
#ifdef OLED_DRIVER_ENABLE

oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  if (!is_keyboard_master())
    { return OLED_ROTATION_180; }  // flips the display 180 degrees if offhand
  return OLED_ROTATION_270;
}

// When you add source files to SRC in rules.mk, you can use functions.
const char *read_layer_state(void);
const char *read_logo(void);
void set_keylog(uint16_t keycode, keyrecord_t *record);
const char *read_keylog(void);
const char *read_keylogs(void);

//const char *read_mode_icon(bool swap);
// const char *read_host_led_state(void);
//void set_timelog(void);
//const char *read_timelog(void);
char wpm[7];

void oled_task_user(void) {
  if (is_keyboard_master()) {
    // If you want to change the display of OLED, you need to change here
    oled_write_ln(read_layer_state(), false);
    oled_write_ln(" wpm", false);
    sprintf(wpm, " %03d \n", get_current_wpm());
    oled_write(wpm, false);

    oled_write_ln(read_keylog(), false);
  
    // oled_write_ln(read_timelog(), false);
   
    // oled_write_ln(read_mode_icon(keymap_config.swap_lalt_lgui), false);
    oled_write_ln(read_keylogs(), false);
    
    //oled_write_ln(read_host_led_state(), false);
    
  } else {
    // oled_write_ln(read_layer_state(), false);
    // oled_write_ln(read_mode_icon(keymap_config.swap_lalt_lgui), false);
    // oled_write_ln(read_keylogs(), false);
    // oled_write_ln(read_host_led_state(), false);
    // oled_write_ln(read_timelog(), false);
    oled_write(read_logo(), false);
  }
}
#endif // OLED_DRIVER_ENABLE

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (record->event.pressed) {
#ifdef OLED_DRIVER_ENABLE
    set_keylog(keycode, record);
    //set_timelog();
#endif
    
  }
  return true;
}